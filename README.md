# Kedro Demo

This is a repository for learning and experimenting with Kedro. I've followed the tutorials on the [Kedro Documentation](https://kedro.readthedocs.io/en/stable/index.html) and also experimented a bit on my own.

## About Kedro

The name Kedro, which derives from the Greek word meaning **center or core**, signifies that this open-source software provides crucial code for **productionizing** advanced analytics projects.

## Kedro Architecture

> We can use Kedro either as a `library` or as a `tool`.

![Kedro Architecture](images/kedro_architecture.png)

### Kedro Library

Kedro library consists of independent units, each responsible for one aspect of computation in a data pipeline:
- `Config Loader` provides utility to parse and load configuration defined in a Kedro project.
- `Pipeline` provides a collection of abstractions to model data pipelines.
- `Runner` provides an abstraction for different execution strategy of a data pipeline.
- `I/O` provides a collection of abstractions to handle I/O in a project, including `DataCatalog` and many `DataSet` implementations.

Take a look at [hello-world\hello-kedro.py](https://gitlab.com/koushikvikram/kedro-demo/-/blob/main/hello-world/hello-kedro.py) for an example of using the Kedro library.

We run this program using the Python command
- `python hello-world\hello-kedro.py`

### Kedro Project

As a **data pipeline developer**, you will interact with a Kedro project, which consists of:
- The **conf/** directory, which contains configuration for the project, such as data catalog configuration, parameters, etc.
- The **src** directory, which contains the source code for the project, including:
    - The **pipelines** directory, which contains the source code for your pipelines.
    - **settings.py** file contains the settings for the project, such as library component registration, custom hooks registration, etc. All the available settings are listed and explained in the project settings chapter.
    - **pipeline_registry.py** file defines the project pipelines, i.e. pipelines that can be run using kedro run --pipeline.
    - **_\__main__\_.py** file serves as the main entry point of the project in package mode.
- **pyproject.toml** identifies the project root by providing project metadata, including:
    - **package_name**: A valid Python package name for your project package
    - **project_name**: A human readable name for your project
    - **project_version**: Kedro version with which the project was generated

### Kedro Starter

Can be used to generate a Kedro project that contains **boilerplate code**. Kedro comes with a list of [official starters](https://github.com/kedro-org/kedro-starters/), but we can also use a custom starter of our choice.

### Kedro framework

Kedro framework serves as the **interface between a Kedro project and Kedro library components**. The major building blocks of the Kedro framework include:
- **Session** is responsible for managing the lifecycle of a Kedro run.
- **Context** holds the configuration and Kedro’s main functionality, and also serves as the main entry point for interactions with core library components.
- **Hooks** defines all hook specifications available to extend Kedro.
- **CLI** defines built-in Kedro CLI commands and utilities to load custom CLI commands from plugins.

### Kedro extension

We can also extend Kedro behaviour in our project using a Kedro extension, which can be a 
- custom starter
- Python library with extra hooks implementations
- extra CLI commands such as Kedro-Viz 
- custom library component implementation

## Kedro's 7 Principles

<details>
    <summary>1. Modularity at the core ️📦</summary>
    Modularity allows for easy construction, flexible arrangements and reusability of components, resulting in an extensible and customisable system. Kedro is built around the idea of enabling modular data engineering and data science code. To make this possible, we take this as our core tenet and make sure Kedro’s own components are modular and independent of each other as much as possible. Each component has clearly defined responsibilities and narrow interfaces. We aim to make most of our components highly decoupled from each other and ensure they can be used on their own.
</details>
<details>
    <summary>2. Grow beginners into experts 🌱</summary>
    Every user is on a learning journey and Kedro aims to be the perfect vehicle for such an adventure. We want Kedro to be loved by users across all different levels of experience. Kedro should be your companion as a beginner, taking your first steps into building data products, or as an expert user, well-seasoned in taking machine-learning models into production.
</details>
<details>
    <summary>3. User empathy without unfounded assumptions 🤝</summary>
    Kedro is designed with the user in mind, but makes no guesses about what the user has in mind. We strive to understand our users without claiming we are one with them. Our users trust us with their time to learn our API and we should make sure we spend their time wisely. All our assumptions should be grounded on extensive experience and hard data.
</details>
<details>
    <summary>4. Simplicity means bare necessities 🍞</summary>
    We believe that simplicity is “attained not when there is no longer anything to add, but when there is no longer anything to take away”, very much like Antoine de Saint-Exupéry’s definition of perfection. Simplicity is hard to achieve, but once achieved it is easy to understand and rely on. In our pursuit of simplicity at Kedro we start by defining it as something composed of small number of parts, with small number of features or functional branches and having very little optionality. Simple things are easy, robust, reliable and loved by everyone. They can be used in countless ways on their own or effortlessly become a part of a more complex system since they are modular by nature.
</details>
<details>
    <summary>5. There should be one obvious way of doing things 🎯</summary>
    Inspired by The Zen of Python, we recommend certain ways of accomplishing tasks. We do this because it allows users to focus on their original problem rather than deal with accidental complexity. That doesn’t mean that it will be impossible to do things using a different way; but, as one becomes more accustomed to Kedro, it will become apparent that there is a preferred way of doing things. Kedro is an opinionated framework, and this is built into its design.
</details>
<details>
    <summary>6. A sprinkle of magic is better than a spoonful of it ✨</summary>
    The declarative nature of Kedro introduces some magic by hiding the imperative implementation details. However, we recognise that this departure from Python’s preference for explicit solutions can be taken too far and quickly spiral into “dark magic”. Dark magic introduces confusion for the users and can make it easy for them to get lost in their own project. That’s why we have a strong preference for common sense over dark magic and making things obvious rather than clever. Nevertheless, magic is sometimes justified if it simplifies how things work. We promise to use it sparingly and only for good.
</details>
<details>
    <summary>7. Lean process and lean product 👟</summary>
    - Kedro started as a small framework which tackles big problems in the delivery of data science projects from inception to production. We fully subscribe to the principles of lean software development and do our best to eliminate waste as much as possible. We favour small incremental changes over big bang deliveries of functionality and in general we strive to achieve more with less.
</details>


## Basic Elements of Kedro

The most basic elements of Kedro are:
1. Node
2. Pipeline
3. DataCatalog
4. Runner

### Node
- **building block of a pipeline**
- **wrapper for a Python function** that 
    - **names the inputs and outputs of that function**
- can be **linked** when 
    - the output of one node is the input of another

### Pipeline
- organises the **dependencies** and **execution order** of a collection of **nodes**
- **connects** inputs and outputs while keeping your **code modular**

### DataCatalog
- It is a Kedro concept
- the **registry** of **all data sources** that the project can use
- maps the names of node inputs and outputs as keys in a `DataSet`
    - `DataSet` - Kedro class that can be specialised for different types of data storage. 
        - Kedro uses a `MemoryDataSet` for data that is simply stored in-memory.

> Kedro provides a [number of different built-in datasets](https://kedro.readthedocs.io/en/stable/kedro.extras.datasets.html) for different file types and file systems so we don’t have to write the logic for reading/writing data.

### Runner
- an object that **runs the pipeline**

<details>
    <summary>hello_kedro.py</summary>

```python
"""Contents of hello_kedro.py"""
from kedro.io import DataCatalog, MemoryDataSet
from kedro.pipeline import node, pipeline
from kedro.runner import SequentialRunner

# Prepare a data catalog
data_catalog = DataCatalog({"my_salutation": MemoryDataSet()})

# Prepare first node
def return_greeting():
    return "Hello"


return_greeting_node = node(return_greeting, inputs=None, outputs="my_salutation")

# Prepare second node
def join_statements(greeting):
    return f"{greeting} Kedro!"


join_statements_node = node(
    join_statements, inputs="my_salutation", outputs="my_message"
)

# Assemble nodes into a pipeline
greeting_pipeline = pipeline([return_greeting_node, join_statements_node])

# Create a runner to run the pipeline
runner = SequentialRunner()

# Run the pipeline
print(runner.run(greeting_pipeline, data_catalog))
```
</details>

Order of execution:
- Kedro first executes `return_greeting_node`. This runs `return_greeting`, which takes no input but outputs the string `"Hello"`.
- The output string is stored in the `MemoryDataSet` named `my_salutation`.
- Kedro then executes the second node, `join_statements_node`. This loads the `my_salutation` dataset and injects it into the `join_statements` function.
- The function joins the input salutation with `"Kedro!"` to form the output string `"Hello Kedro!"`
- The output of the pipeline is returned in a dictionary with key `my_message`.

## Ways to create a Kedro Project

1. Import elements from the Kedro library. Refer to [hello-world\hello-kedro.py](https://gitlab.com/koushikvikram/kedro-demo/-/blob/main/hello-world/hello-kedro.py). This file can be run using `python hello-world\hello-kedro.py`
2. `kedro new`
    - Create a new project interactively. Run `kedro new` from the command line in the folder where we want to create the project and provide the following information:
        - Project Name
        - Repository Name
        - Python Package Name
3. `kedro new --config config.yml`
    - Create a new project from a configuration file. Run `kedro new --config config.yml` from the command line, where `config.yml` is a file that contains the following content:
        - `output_dir`: ~/code
        - `project_name`: Get Started
        - `repo_name`: get-started
        - `python_package`: get_started
4. `kedro new --starter=<starter-name>`
    - Create project from starter template. Run `kedro new --starter=<starter-name>`
        - List of [starter-name](https://kedro.readthedocs.io/en/stable/get_started/starters.html#list-of-official-starters) 
        - To see a list of all starters, run the command, `kedro starter list`
5. `kedro new --config=<config-file.yml> --starter=<starter-name>`
    - Create a starter with a configuration file. Run `kedro new --config=<config-file.yml> --starter=<starter-name>`
6. `kedro new --starter=standalone-datacatalog`
    - Create a project from a standalone DataCatalog. Run `kedro new --starter=standalone-datacatalog`. Then, create a new Kedro project and copy the `conf` and `data` folders from the standalone datacatalog into the new project. 

## Kedro Project Development Workflow

![Typical Workflow](images/typical_workflow.png)

1. Set up the project template
    - Create a new project with `kedro new`
    - Install project dependencies with `pip install -r src/requirements.txt`
    - Configure the following in the `conf` folder:
        - Logging
        - Credentials
        - Any other sensitive/personal content
2. Set up the data
    - Add data to the `data/` folder
    - Reference all datasets for the project in `conf/base/catalog.yml`
3. Create the pipeline
    - Create the data transformation steps as Python functions
    - Construct the pipeline by adding your functions as nodes
    - Choose how to run the pipeline: sequentially or in parallel
4. Package the project
    - Build the project documentation
    - Package the project for distribution

## Applying the Workflow with an Example

```bash
# project setup
kedro new
cd <PROJECT_DIRECTORY>
kedro build-reqs

# data setup

```