from kedro.io import DataCatalog, MemoryDataSet
from kedro.pipeline import node, pipeline
from kedro.runner import SequentialRunner


# create the data catalog - similar to a Python dictionary
# Datasets are connectors 
# there are other type of datasets that we can use:
# https://kedro.readthedocs.io/en/stable/kedro.extras.datasets.html
data_catalog = DataCatalog({
    "first_name": MemoryDataSet(data="Koushik"),
    "last_name": MemoryDataSet(data="Vikram")
    })

# define functions
def make_name(first_name, last_name):
    return f"{first_name} {last_name}"

def hello(name):
    return f"Hello, {name}!"

def question():
    return "How are you?"

# create nodes
make_name_node = node(
    func=make_name,
    inputs=["first_name", "last_name"],
    outputs="full_name"
    )
greeting_node = node(
    func=hello,
    inputs="full_name",
    outputs="greeting"
    )
question_node = node(
    func=question,
    inputs=None,
    outputs="question"
    )

# make a pipeline out of the nodes
hello_world_pipeline = pipeline([make_name_node, greeting_node, question_node])

# create a runner and run the pipeline
# https://kedro.readthedocs.io/en/stable/nodes_and_pipelines/run_a_pipeline.html#sequentialrunner
runner = SequentialRunner()
pipeline_output = runner.run(hello_world_pipeline, data_catalog)

# doesn't print full_name although it is an output 
# of the first node because it's being fed as input to the 2nd node
print(pipeline_output)
